# pretty printers pre kniznicu glm
from dumper import *

def vec2_str(val):
	return '(%s, %s)' % (val['x'], val['y'])

def vec3_str(val):
	return '(%s, %s, %s)' % (val['x'], val['y'], val['z'])

def vec4_str(val):
	return '(%s, %s, %s, %s)' % (val['x'], val['y'], val['z'], val['w'])

# {i|u}vec2	
def qdump__glm__detail__tvec2(d, val):
	d.putValue(vec2_str(val))
	d.putNumChild(2)
	if d.isExpanded():
		with Children(d):
			d.putSubItem('x', val['x'])
			d.putSubItem('y', val['y'])

# {i|u}vec3
def qdump__glm__detail__tvec3(d, val):
	d.putValue(vec3_str(val))
	d.putNumChild(3)
	if d.isExpanded():
		with Children(d):
			d.putSubItem('x', val['x'])
			d.putSubItem('y', val['y'])
			d.putSubItem('z', val['z'])

# {i|u}vec4
def qdump__glm__detail__tvec4(d, val):
	d.putValue(vec4_str(val))
	d.putNumChild(4)
	if d.isExpanded():
		with Children(d):
			d.putSubItem('x', val['x'])
			d.putSubItem('y', val['y'])
			d.putSubItem('z', val['z'])
			d.putSubItem('w', val['w'])

# quat
def qdump__glm__detail__tquat(d, val):
	d.putValue('(%s, (%s, %s, %s))' % (val['w'], val['x'], val['y'], val['z']))
	d.putPlainChildren(val)

# mat3
def qdump__glm__detail__tmat3x3(d, val):
	# column vectors
	c0 = val['value'][0]
	c1 = val['value'][1]
	c2 = val['value'][2]
	d.putValue('[%s, %s, %s]' % (vec3_str(c0), vec3_str(c1), vec3_str(c2)))
	d.putNumChild(3)
	if d.isExpanded():
		with Children(d):
			d.putSubItem('column 0', c0)
			d.putSubItem('column 1', c1)
			d.putSubItem('column 2', c2)

# mat4
def qdump__glm__detail__tmat4x4(d, val):
	# column vectors
	c0 = val['value'][0]
	c1 = val['value'][1]
	c2 = val['value'][2]
	c3 = val['value'][3]
	d.putValue('[%s, %s, %s, %s]' % (vec4_str(c0), vec4_str(c1), vec4_str(c2), vec4_str(c3)))
	d.putNumChild(4)
	if d.isExpanded():
		with Children(d):
			d.putSubItem('column 0', c0)
			d.putSubItem('column 1', c1)
			d.putSubItem('column 2', c2)
			d.putSubItem('column 3', c3)

# for testing purposes only ...
def qdump__foo(d, val):
	d.putValue('(a=%s, b=%s)' % (val['a'], val['b']))
	d.putNumChild(2)
	if d.isExpanded():
		with Children(d):
			d.putSubItem('a', val['a'])
			d.putSubItem('b', val['b'])
