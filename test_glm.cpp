#include <memory>
#include <complex>
#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

using namespace glm;
using std::unique_ptr;
using std::complex;
using std::string;

// test structure from 'personaltypes.py'
template <typename U, typename V>
struct MapNode
{
	U key;
	V data;
};

struct foo
{
	int a, b;
};

int main(int argc, char * argv[])
{
	vec2 v2{1.2, 2.12};
	ivec2 i2{5,-6};
	uvec2 u2{1,2};
	vec3 v3{1,2,3.04};
	ivec3 i3{1,2,3};
	vec4 v4{1.001, 0.01, 1000, 4};
	ivec4 i4{-1,-2,-3,-4};
	uvec4 u4{6,7,8,9};
	quat q{1,2,3,4};
	mat3 m3{1};
	mat4 m4{
		1,1,1,1,
		2,2,2,2,
		3,3,3,3,
		4,4,4,4};
	foo f;
	f.a = 10;
	f.b = 11;
	unique_ptr<int> p{new int{10}};
	complex<float> c{3, 9};
	MapNode<string, int> node;
	node.key = "hello";
	node.data = 123;
	return 0;
}
